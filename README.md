# BLURT Account Creation

ESM Typscript [BLURT blockchain](https://blurt.blog/) account creation via command line (inquirer)

## Getting Started

### Installation

clone the repository

```bash
~/$ git clone https://gitlab.com/beblurt/blurt-account-creation.git
```

go in the directory

```bash
~$ cd blurt-account-creation
```

install dependencies

```bash
~/blurt-account-creation$ npm install
```

## Usage

Run it

```bash
~/blurt-account-creation$ npm start
```

Answer the questions

- *Run in debug mode? (y/N)* | default "no"
- *What BLURT USERNAME would you like to have?* | put a username without @
- *Which BLURT RPC node to use?* | default https://rpc.blurt.world
- *What is the BLURT account that will create this username?* | your account without @
- *What is the ACTIVE KEY of the CREATOR account?* | your account private Active key

Fees Question

- *? Account creation fees is X BLURT Continue?* | Retrieve the actual fees (X BLURT) for account creation from the BLURT Blockchain and ask you if you want to continue, default "yes"

Result

in case of success it will show you the master key, all keys (owner, active, posting, memo) and the transaction ID & the block number in the console:

```
mynewaccount master password: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
{
  keys: {
    owner: {
      private: '5xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
      public: 'BLTxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    },
    active: {
      private: '5xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
      public: 'BLTxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    },
    posting: {
      private: '5xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
      public: 'BLTxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    },
    memo: {
      private: '5xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
      public: 'BLTxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    }
  }
}
[response] {
  id: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
  block_num: xxxxxxxx,
  trx_num: x,
  expired: false
}
account created with success: mynewaccount

```


## Author

@beblurt (https://blurt.blog/@beblurt)

## License

 Copyright (C) 2022  IMT ASE Co., LTD
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.