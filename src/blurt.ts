/** 
 * @project blurt-account-creation
 * @author  @beblurt (https://blurt.blog/@beblurt)
 * @license
 * Copyright (C) 2022  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import assert from 'assert'

/** Require */
import inquirer, { Answers } from 'inquirer'
import { Client, PrivateKey, Asset } from '@blurtopian/dblurt'
import { createHash } from 'crypto'

/**
 * address prefix (public key).
 */
export const addressPrefix = 'BLT'

/**
 * BLURT chain ID.
 */
export const chainId = 'cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f'

type KEYS = {
  owner:   { private: string, public: string }
  active:  { private: string, public: string }
  posting: { private: string, public: string }
  memo:    { private: string, public: string }
}

type CREATE_ACCOUNT = {
  creator:          string
  new_account_name: string
  fee:              string|Asset
  owner: {
    weight_threshold: number
    account_auths:    Array<[ string, number ]>|[],
    key_auths:        Array<[ string, number ]>,
  }
  active: {
    weight_threshold: number
    account_auths:    Array<[ string, number ]>|[],
    key_auths:        Array<[ string, number ]>,
  }
  posting: {
    weight_threshold: number
    account_auths:    Array<[ string, number ]>|[],
    key_auths:        Array<[ string, number ]>,
  }
  memo_key:      string
  json_metadata: string
}
type OP_CREATE_ACCOUNT = [ 'account_create', CREATE_ACCOUNT]

/**
* Return sha256 hash of input.
*/
const sha256 = (input: Buffer | string): Buffer => {
  return createHash('sha256')
    .update(input)
    .digest()
}

/**
 * Create claimed account.
 */
export const createClaimedAccount = (username: string, password: string, rpc: string, creatorAccount: string, creatorAccountActiveKey: string): Promise<void> => {
  return new Promise(async (resolve, reject) => {
    try {

      const client    = new Client([rpc], { addressPrefix, chainId })
      const chainProperties = await client.database.getChainProperties()
      console.log('\x1b[1;35m%s\x1b[0m', `account_creation_fee:`, chainProperties.account_creation_fee)

      const answers: Answers = await inquirer.prompt([
        {
          type: 'confirm',
          name: 'account_creation_fee',
          message: `Account creation fees is ${chainProperties.account_creation_fee} Continue?`,
          default: true,
        },
      ])
      assert(answers['account_creation_fee'], `Process aborted by the user!`)

      const masterPassword = sha256(password)
      console.log('\x1b[1;33m%s\x1b[0m', `${username} master password:`, masterPassword.toString('hex'))

      const ownerKey   = PrivateKey.fromLogin(username, masterPassword.toString('hex'), 'owner')
      const activeKey  = PrivateKey.fromLogin(username, masterPassword.toString('hex'), 'active')
      const postingKey = PrivateKey.fromLogin(username, masterPassword.toString('hex'), 'posting')
      const memoKey    = PrivateKey.fromLogin(username, masterPassword.toString('hex'), 'memo')

      const keys: KEYS = {
        owner: { 
          private: ownerKey.toString(), 
          public:  ownerKey.createPublic(addressPrefix).toString() 
        },
        active:  { 
          private: activeKey.toString(), 
          public:  activeKey.createPublic(addressPrefix).toString() 
        },
        posting: { 
          private: postingKey.toString(), 
          public:  postingKey.createPublic(addressPrefix).toString() 
        },
        memo:    { 
          private: memoKey.toString(), 
          public:  memoKey.createPublic(addressPrefix).toString() 
        },
      }
      console.log({ keys })

      /** Prepare Blurt Blockchain Operation */
      const json_metadata = {
        profile: {
          name: username,
          about: `@${username}`,
          website: '',
          cover_image:   '',
          profile_image: ''
        },
        app: `beblurt/${process.env['VERSION']}`
      }

      const op: OP_CREATE_ACCOUNT = [
        'account_create',
        {
          creator: creatorAccount,
          new_account_name: username,
          fee: chainProperties.account_creation_fee,
          owner: {
            weight_threshold: 1,
            account_auths: [],
            key_auths: [[ keys.owner.public, 1 ]],
          },
          active: {
            weight_threshold: 1,
            account_auths: [],
            key_auths: [[ keys.active.public, 1 ]],
          },
          posting: {
            weight_threshold: 1,
            account_auths: [],
            key_auths: [[ keys.posting.public, 1 ]],
          },
          memo_key:      keys.memo.public,
          json_metadata: JSON.stringify(json_metadata),
          // extensions:    [],
        }
      ]
     
      const keyActive = PrivateKey.fromString(creatorAccountActiveKey)
      const response = await client.broadcast.sendOperations([op], keyActive)
      console.log('[response]', response)

      resolve()
    } catch (e) {
      reject(e)
    }
  })
}
