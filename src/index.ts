/** 
 * @project blurt-account-creation
 * @author  @beblurt (https://blurt.blog/@beblurt)
 * @license
 * Copyright (C) 2022  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import assert from 'assert'

/** Require */
import inquirer, { Answers } from 'inquirer'
import Chance from 'chance'

import { createClaimedAccount } from './blurt.js'

/** Blurt account format verification function */
const isUsernameValid = async (value: string): Promise<boolean> => {
  try {
    assert(value !== undefined && value !== null && value.toString().length !== 0, `${value} can't be tested!`)
    let result = false
    const re = new RegExp('^(?=.{3,16}$)[a-z][0-9a-z\-]{1,}[0-9a-z]([\.][a-z][0-9a-z\-]{1,}[0-9a-z]){0,}$')
    result = re.test(value)
    if(result) {
      return true
    } else {
      return false
    } 
  } catch (e) {
    return false
  }
}

/** Start */
const start = async (): Promise<void> => {
  try {
    const answers: Answers = await inquirer.prompt([
      {
        type: 'confirm',
        name: 'debugMode',
        message: 'Run in debug mode?',
        default: false,
      },
      {
        type: 'input',
        name: 'username',
        message: 'What BLURT USERNAME would you like to have?',
      },
      {
        type: 'input',
        name: 'rpcNode',
        message: 'Which BLURT RPC node to use?',
        default: 'https://rpc.blurt.world',
      }, 
      {
        type: 'input',
        name: 'creatorAccount',
        message: 'What is the BLURT account that will create this username?',
      },
      {
        type: 'input',
        name: 'creatorAccountActiveKey',
        message: 'What is the ACTIVE KEY of the CREATOR account?',
      },
    ])

    /** set Debug Mode if asked */
    if(answers['debugMode']) {
      process.env['APP_DEBUG'] = 'ALL'
    }

    assert(isUsernameValid(answers['username']), `${answers['username']} is not a valid username!`)

    /** Generate random hash password */
    const chance   = new Chance()
    const password = chance.hash({ length: 32 })

    /** Call the create function */
    await createClaimedAccount(answers['username'], password, answers['rpcNode'], answers['creatorAccount'], answers['creatorAccountActiveKey'])
    console.log('\x1b[\x1b[1;36m%s\x1b[0m', 'account created with success:', answers['username'])
  } catch (e) {
    if (e instanceof Error) {
      console.log('\x1b[31m%s\x1b[0m', `Error: ${e.message}`)
      if (process.env['APP_DEBUG']) console.log('\x1b[33m%s\x1b[0m', 'Stack:', e.stack ? e.stack : 'No info')
    } else {
      if (process.env['APP_DEBUG']) console.log(e)
    }
  }
}
start()